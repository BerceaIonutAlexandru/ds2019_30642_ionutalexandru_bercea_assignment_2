package com.bercea.tutorial.in28minutes;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import com.bercea.tutorial.in28minutes.model.CaregiverPatients;
import com.bercea.tutorial.in28minutes.model.Patient;
import com.bercea.tutorial.in28minutes.model.User;
import com.bercea.tutorial.in28minutes.service.CaregiverPatientsService;
import com.bercea.tutorial.in28minutes.service.CaregiverService;
import com.bercea.tutorial.in28minutes.service.PatientService;
import com.bercea.tutorial.in28minutes.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class UserServiceCommandLineRunner implements CommandLineRunner {
    /*@Override
    public void run(String... args) throws Exception {

    }*/

    private static final Logger log = LoggerFactory.getLogger(UserServiceCommandLineRunner.class);

    @Autowired
    UserService userService;
    @Autowired
    PatientService patientService;
    @Autowired
    CaregiverService caregiverService;
    @Autowired
    CaregiverPatientsService caregiverPatientsService;

    @Override
    public void run(String... args) throws Exception {
        User user = new User("Dan","Admin");
        User user2 = new User("Daniel","Admin");

        Patient p1 = new Patient("Alin", new Date(), "male", "Craiova");
        Patient p2= new Patient("Alin1", new Date(), "male", "Craiova");
        Patient p3 = new Patient("Alin2", new Date(), "male", "Craiova");
        Patient p4 = new Patient("Alin3", new Date(), "male", "Craiova");


       /* log.info("New user is created " + p1);
        log.info("New user is created " + p2);
        log.info("New user is created " + p3);
        log.info("New user is created " + p4);*/

        List<Long> ids = new ArrayList<Long>();
        ids.add(11L);
        ids.add(12L);
        ids.add(13L);
        ids.add(14L);
        //log.info("Patients: " + patientService.getAllPatientsById(ids));

        Caregiver caregiver = new Caregiver("Petronela", new Date(), "female", "Petrosani");

       /* System.out.println(caregiverService.existsCaregiver(caregiver)+"  id = "+caregiver.getId());
        for(Caregiver c: caregiverService.getAllCaregivers()){
            System.out.println(c.getName()+""+c.getId());
        }*/

        System.out.println(caregiverPatientsService.getCaregiverPatient(caregiver));

    }

}
