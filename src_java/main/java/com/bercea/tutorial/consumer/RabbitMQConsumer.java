package com.bercea.tutorial.in28minutes.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
class RabbitMQConsumer implements MessageListener {
    @Override
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message) {
        String mess =new String(message.getBody());

        String[] messSplitedByDQuote = mess.split("\"");
        //yyyy-MM-dd HH:mm:ss
        String[] messFinalSplitted = messSplitedByDQuote[1].split("\\\\t\\\\t");
        try {
            Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .parse(messFinalSplitted[0]);
            System.out.println("start date = " + startDate);

            Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .parse(messFinalSplitted[1]);
            System.out.println("end date = " + endDate);

            long diff = endDate.getTime() - startDate.getTime();
            long diffHours = diff;
            System.out.println("difference " + diff);
            if((diff>=3600000 && messFinalSplitted[2].equals("Showering")) || ((diff > 43200000) && (messFinalSplitted[2].equals("Sleeping") || messFinalSplitted[2].equals("Leaving")))){
                System.out.println("This activity took too long "+messFinalSplitted[2]);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        System.out.println("Consuming message - " + new String(message.getBody()));
    }
}

