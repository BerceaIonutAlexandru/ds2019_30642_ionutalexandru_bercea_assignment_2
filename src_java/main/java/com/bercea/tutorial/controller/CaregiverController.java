package com.bercea.tutorial.in28minutes.controller;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import com.bercea.tutorial.in28minutes.model.CaregiverPatients;
import com.bercea.tutorial.in28minutes.producer.RabbitMQProducer;
import com.bercea.tutorial.in28minutes.repository.CaregiverPatientsRepository;
import com.bercea.tutorial.in28minutes.service.CaregiverPatientsService;
import com.bercea.tutorial.in28minutes.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import com.bercea.tutorial.in28minutes.filereader.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
public class CaregiverController {
    @Autowired
    CaregiverService caregiverService;
    @Autowired
    CaregiverPatientsService caregiverPatientsService;
    @Autowired
    CaregiverPatientsRepository caregiverPatientsRepository;

    @Autowired
    RabbitMQProducer rabbitMQSender;

    @Autowired
    FileReader fileReader;

    @GetMapping("/caregivers-patients")
    public List<CaregiverPatients> getAll(){
        return (List<CaregiverPatients>) caregiverPatientsService.getAll();
    }

    @GetMapping("/caregivers-patients/{id}")
    public Optional<CaregiverPatients> getAllById(@PathVariable long id){
        return caregiverPatientsRepository.findById(id);
    }

    @GetMapping(value="/caregivers/send")
    public String sendMessage() throws IOException, InterruptedException {
        for(int i=0; i<fileReader.readFile().size(); i++){
            rabbitMQSender.send(fileReader.readFile().get(i));
            System.out.println("Activity no. " + i + "sent");
        }
        return "Message sent from web successfully";
    }
}
