package com.bercea.tutorial.in28minutes.controller;

import com.bercea.tutorial.in28minutes.model.Doctor;
import com.bercea.tutorial.in28minutes.repository.DoctorRepository;
import com.bercea.tutorial.in28minutes.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class DoctorController {
    @Autowired
    private DoctorService doctorService;

    @Autowired
    private DoctorRepository doctorRepository;

    @GetMapping("/doctors")
    public List<Doctor> getAllDoctors(){
        return (List<Doctor>) doctorService.getAllDoctors();
    }
    @GetMapping("/doctors/{id}")
    public Optional<Doctor> getDoctorById(@PathVariable long id){
        return doctorService.getDoctorById(id);
    }
    @PostMapping
    public void insertDoctor(@RequestBody Doctor doctor){
        doctorService.insertDoctor(doctor);
    }
    @PutMapping
    public void updateDoctor(@RequestBody Doctor doctor){
        doctorService.updateDoctor(doctor);
    }
    @DeleteMapping("/doctors/{id}")
    public void deleteDoctorById(@PathVariable long id){
        doctorRepository.deleteById(id);
    }

}
