package com.bercea.tutorial.in28minutes.controller;

import com.bercea.tutorial.in28minutes.model.Patient;
import com.bercea.tutorial.in28minutes.repository.CaregiverRepository;
import com.bercea.tutorial.in28minutes.repository.PatientRepository;
import com.bercea.tutorial.in28minutes.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class PatientController {

    @Autowired
    PatientRepository patientRepository;
    @Autowired
    PatientService patientService;
    @Autowired
    CaregiverRepository caregiverRepository;

    @GetMapping("/patients")
    public List<Patient> allPatients(){
        return patientService.getAllPatients();
    }

   /* @GetMapping("/patients/{id}")
    public Optional<Patient> getPatientById(@PathVariable long id){
        return patientService.getPatientById(id);
    }*/
    @PutMapping("/patients")
    public Patient updatePatientById(@RequestBody Patient patient){
        return patientService.updatePatientById(patient);
    }

    @PostMapping("/patients")
    public void insertPatient(@RequestBody Patient patient){
        patientService.insertPatient(patient);
    }

    @DeleteMapping("patients/{id}")
    public void deletePatientById(@PathVariable long id){
        patientRepository.deleteById(id);
    }
}
