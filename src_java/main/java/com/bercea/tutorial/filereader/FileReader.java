package com.bercea.tutorial.in28minutes.filereader;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileReader {

    public List<String> readFile() throws IOException {
        java.io.FileReader fr = new java.io.FileReader("C:\\Users\\Ionut\\Desktop\\SD\\rabbitmq2\\src\\activity.txt");
        BufferedReader br = new BufferedReader(fr);

        List<String> activities = new ArrayList<String>();
        String str;
        while((str=br.readLine())!=null){
            activities.add(str);
        }
        br.close();
        return activities;
    }
}
