package com.bercea.tutorial.in28minutes.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Caregiver {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private Date birthDate;

    private String gender;

    private String Address;

    private long role;

    @OneToOne
    private Patient patient;

    public Caregiver() {
    }

    public Caregiver(String name, Date birthDate, String gender, String address) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        Address = address;
        //this.patients = patients;
        this.role = 2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public long getId() {
        return id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public void setId(long id) {
        this.id = id;
    }
}
