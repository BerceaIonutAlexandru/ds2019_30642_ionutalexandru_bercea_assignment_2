package com.bercea.tutorial.in28minutes.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "caregiver_patients")
public class CaregiverPatients {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long caregiver_id;
    private long patient_id;

    public CaregiverPatients() {
    }

    public long getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(long patient_id) {
        this.patient_id = patient_id;
    }

    public CaregiverPatients(long caregiver_id, long patient_id) {
        this.caregiver_id = caregiver_id;
        this.patient_id = patient_id;
    }


    public long getCaregiver_id() {
        return caregiver_id;
    }

    public void setCaregiver_id(long caregiver_id) {
        this.caregiver_id = caregiver_id;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverPatients that = (CaregiverPatients) o;
        return caregiver_id == that.caregiver_id ;
    }

}
