package com.bercea.tutorial.in28minutes.repository;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import com.bercea.tutorial.in28minutes.model.CaregiverPatients;
import com.bercea.tutorial.in28minutes.model.Patient;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CaregiverPatientsRepository extends CrudRepository<CaregiverPatients, Long> {

}
