package com.bercea.tutorial.in28minutes.repository;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaregiverRepository extends CrudRepository<Caregiver, Long> {
}
