package com.bercea.tutorial.in28minutes.repository;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import com.bercea.tutorial.in28minutes.model.Doctor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Long> {
}
