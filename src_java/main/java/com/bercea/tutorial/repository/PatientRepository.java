package com.bercea.tutorial.in28minutes.repository;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import com.bercea.tutorial.in28minutes.model.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {
}
