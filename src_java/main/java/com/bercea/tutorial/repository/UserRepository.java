package com.bercea.tutorial.in28minutes.repository;

import com.bercea.tutorial.in28minutes.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {

}
