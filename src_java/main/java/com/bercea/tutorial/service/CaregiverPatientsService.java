package com.bercea.tutorial.in28minutes.service;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import com.bercea.tutorial.in28minutes.model.CaregiverPatients;
import com.bercea.tutorial.in28minutes.model.Patient;
import com.bercea.tutorial.in28minutes.repository.CaregiverPatientsRepository;
import com.bercea.tutorial.in28minutes.repository.CaregiverRepository;
import com.bercea.tutorial.in28minutes.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CaregiverPatientsService {
    @Autowired
    private CaregiverPatientsRepository caregiverPatientsRepository;
    @Autowired
    private CaregiverService caregiverService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private CaregiverRepository caregiverRepository;
    @Autowired
    private PatientRepository patientRepository;
    //i will use this to get patients by id

    public Iterable<CaregiverPatients> getAll(){
        return caregiverPatientsRepository.findAll();
    }

    public void insertCaregiverPatient(Caregiver caregiver, Patient patient){
        CaregiverPatients caregiverPatients = new CaregiverPatients(caregiver.getId(), patient.getId());
        if(caregiverService.existsCaregiver(caregiver)){
          //  caregiverPatientsRepository.save(caregiverPatients);
        }else{
            caregiverRepository.save(caregiver);
            caregiverPatientsRepository.save(caregiverPatients);
        }
    }

    public Optional<CaregiverPatients> getCaregiverPatient(Caregiver caregiver){
        return caregiverPatientsRepository.findById(caregiver.getId());
    }

}
