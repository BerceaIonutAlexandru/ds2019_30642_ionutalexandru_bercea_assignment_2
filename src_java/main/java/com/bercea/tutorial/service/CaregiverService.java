package com.bercea.tutorial.in28minutes.service;

import com.bercea.tutorial.in28minutes.model.Caregiver;
import com.bercea.tutorial.in28minutes.model.Patient;
import com.bercea.tutorial.in28minutes.repository.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaregiverService {
    @Autowired
    private CaregiverRepository caregiverRepository;

    public List<Caregiver> getAllCaregivers(){
        return (List<Caregiver>)caregiverRepository.findAll();
    }

    public boolean existsCaregiver(Caregiver caregiver){
        List<Caregiver> caregivers = (List<Caregiver>) caregiverRepository.findAll();
        boolean exists = false;
        for(Caregiver c:caregivers){
            if(c.getName().equals(caregiver.getName())){
               return exists = true;
            }else exists =  false;
        }
        return exists;
    }
    public void insertCaregiver(Caregiver caregiver){
        caregiverRepository.save(caregiver);
    }

}
