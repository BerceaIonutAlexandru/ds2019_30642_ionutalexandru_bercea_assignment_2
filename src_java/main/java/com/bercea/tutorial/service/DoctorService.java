package com.bercea.tutorial.in28minutes.service;

import com.bercea.tutorial.in28minutes.model.Doctor;
import com.bercea.tutorial.in28minutes.model.Patient;
import com.bercea.tutorial.in28minutes.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.Doc;
import java.util.List;
import java.util.Optional;

@Service
public class DoctorService {
    @Autowired
    private DoctorRepository doctorRepository;

    public List<Doctor> getAllDoctors(){
        return (List<Doctor>) doctorRepository.findAll();
    }
    public Optional<Doctor> getDoctorById(long id){
        return doctorRepository.findById(id);
    }
    public void insertDoctor(Doctor doctor){ doctorRepository.save(doctor); }
    public Doctor updateDoctor(Doctor doctor) {
        return doctorRepository.save(doctor);
    }
}
