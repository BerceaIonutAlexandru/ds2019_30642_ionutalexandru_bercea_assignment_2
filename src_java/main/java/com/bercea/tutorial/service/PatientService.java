package com.bercea.tutorial.in28minutes.service;

import com.bercea.tutorial.in28minutes.model.Patient;
import com.bercea.tutorial.in28minutes.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;

    public List<Patient> getAllPatients(){
        return (List<Patient>) patientRepository.findAll();
    }

    public void insertPatient(Patient patient){
        patientRepository.save(patient);
    }

    public Patient updatePatientById(Patient patient) {
        return patientRepository.save(patient);
    }

    public List<Optional<Patient>> getAllPatientsById(List<Long> patientsId){
      //  List<Patient> patients = (List<Patient>) patientRepository.findAll();
        List<Optional<Patient>> foundPatientsById = new ArrayList<>();
        for(int i=0; i<patientsId.size(); i++){
            if(patientRepository.findById(patientsId.get(i))!=null){
                foundPatientsById.add(patientRepository.findById(patientsId.get(i)));
            }
        }
        return foundPatientsById;
    }
}
