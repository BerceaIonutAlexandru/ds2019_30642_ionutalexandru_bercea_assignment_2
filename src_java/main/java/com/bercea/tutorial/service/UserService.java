package com.bercea.tutorial.in28minutes.service;

import com.bercea.tutorial.in28minutes.model.User;
import com.bercea.tutorial.in28minutes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public void insert(User user){
        userRepository.save(user);
    }

}
